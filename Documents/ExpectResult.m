% for high frequency
x =  [0.5 1 1.5];
figure('rend', 'painters', 'pos',[0 0 500 600])
hold on
% short SOA
plot(x, [0.8 0.7 0.6], 'Marker', '.', 'MarkerSize', 40, 'LineStyle', '-', ...
    'LineWidth', 3, 'Color', [255 143 171]/255)
% long SOA
plot(x, [0.6 0.75 0.9], 'Marker', '.', 'MarkerSize', 40, 'LineStyle', '-', ...
    'LineWidth', 3, 'Color', [31 91 221]/255)
ylabel('Proportion correct')
legend({'Short SOA', 'Long SOA'}, 'location', 'best')
xlim([0 2])
ylim([0.5 1])
set(gca, 'XTick', x, 'XTickLabel', {'Cong.','Neutral','Incong.'}, 'fontsize', 28, 'LineWidth', 2.5)
xtickangle(30)
title('High Spatial Frequency')
saveas(gca, './ExpectResult/HighFreq.png')

% for low frequency
x =  [0.5 1 1.5];
figure('rend', 'painters', 'pos',[0 0 500 600])
hold on
% short SOA
plot(x, [0.75 0.7 0.65], 'Marker', '.', 'MarkerSize', 40, 'LineStyle', '-', ...
    'LineWidth', 3, 'Color', [255 143 171]/255)
% long SOA
plot(x, [0.7 0.75 0.8], 'Marker', '.', 'MarkerSize', 40, 'LineStyle', '-', ...
    'LineWidth', 3, 'Color', [31 91 221]/255)
ylabel('Proportion correct')
legend({'Short SOA', 'Long SOA'}, 'location', 'best')
xlim([0 2])
ylim([0.5 1])
set(gca, 'XTick', x, 'XTickLabel', {'Cong.','Neutral','Incong.'}, 'fontsize', 28, 'LineWidth', 2.5)
xtickangle(30)
title('Low Spatial Frequency')
saveas(gca, './ExpectResult/LowFreq.png')