## Exogenous attention on spatial frequency

### Barbot et al. 2012

* Experimental procedure

  * stimulus has a 4 &deg; envelope

  * displayed in periphery 5 &deg; away from the center

  * exogenous cue was uninformative and endogenous cue was informative 

  * trials discarded if outside the central fixation area subtending 2&deg; visual angle

     <img src="./BarbotEtAl12/stimulus.png" align = "center" alt="stimulus" style="zoom: 33%;" /><img src="./BarbotEtAl12/paradigm.png" alt="paradigm" style="zoom:31%;" />           

* Exogenous attention results

  * no effect from cueing in *low* SF condition 

  * enhanecment in valid trials and inhibition in invalid trials for *high* SF condition

    <img src="./BarbotEtAl12/exo.png" align="center" alt="ExoAttn" style="zoom: 50%;" />

* Endogenous attention results

  * enhancement in valid trials and inhibition in invalid trials were observed for both low and high SF
  * affects 2nd-order contrast regardless of spatial frequency

  <img src="./BarbotEtAl12/endo.png" alt="endo" style="zoom:50%;" />

  



















