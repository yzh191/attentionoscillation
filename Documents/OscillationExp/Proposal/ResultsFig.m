

%%Time specifications:
   Fs = 100;                   % samples per second
   dt = 1/Fs;                   % seconds per sample
   StopTime = 0.7;             % seconds
   t = (0:dt:StopTime-dt)';     % seconds
   %%Sine wave:
   Fc = 4;                     % hertz
   
   %% main oscillation
   figure('rend','painters','pos',[0 0 600 400]); hold on
   % plot congruent
   x = sin(2*pi*Fc*t);
   noise = rand(1, length(x));
   plot(t,x + noise', '-b', 'LineWidth', 2);
   
  % plot incongruent
   x = cos(2*pi*Fc*t);
   noise = rand(1, length(x));
   plot(t, x + noise', '-r', 'LineWidth', 2);
   xlabel('SOA (s)')
   ylabel('Proportion correct')
   zoom xon;
   yticks(-2:1:2)
   yticklabels(0.6:0.1:1)
   xlim([0 0.7])
   xticks(0:0.2:0.7)
   set(gca, 'LineWidth', 2, 'FontSize', 20)
   legend({'Cong.', 'Incong.'})
   
   % saveas(gca, './Result.svg');
   
   %% near vs. far
   figure('rend','painters','pos',[0 0 600 400]); hold on
   % plot congruent
   x = cos(2*pi*1*t);
   plot(t, x', 'b', 'LineWidth', 2);
   plot(t + 0.2, x', 'r', 'LineWidth', 2);
   yticks(-2:1:3)
   yticklabels(0.6:0.1:1)
   xlim([0 0.7])
   xticks(0:0.2:0.7)
   set(gca, 'LineWidth', 2, 'FontSize', 20)
   legend({'Near', 'Far'})
   xlabel('SOA (s)')
   ylabel('Proportion correct')
   saveas(gca, './Distance.svg')