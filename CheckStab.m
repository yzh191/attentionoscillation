for i = 1:length(data.x)
    hold on;
    xStab = plot(data.stream00{i}.ts, data.stream00{i}.data*.5305);
    x = plot(data.x{i});
    yStab = plot(data.stream01{i}.ts, data.stream01{i}.data*.5305);
    y = plot(data.y{i});
    xlabel('sample');
    ylabel('pixel');
    
    legend([x, xStab, y, yStab], 'x', 'xStab', 'y', 'yStab', 'location', 'best')
    set(gca, 'linewidth', 2, 'fontsize', 20, 'Layer','top')
    saveas(gca, sprintf('./IndvFigs/StabTrace%i.png', i))
    input ''
    cla
end