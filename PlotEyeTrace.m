function plotEyeTrace(vt, i)
hold on
yLimit = [-30 30];
% plot stabilized traces
% xStab = double(vt{i}.xStab.stream) .* vt{i}.pxAngle;
% xt = double(vt{i}.xStab.ts);
% yStab = double(vt{i}.yStab.stream) .* vt{i}.pxAngle;
% yt = double(vt{i}.yStab.ts);
% x2 = plot(xt, xStab, 'LineWidth', 1.5);
% y2 = plot(yt, yStab, 'LineWidth', 1.5);

xx = double(vt{i}.x.position) + vt{i}.xOffset * vt{i}.pxAngle;
yy = double(vt{i}.y.position) + vt{i}.yOffset * vt{i}.pxAngle;
x = plot(xx, 'LineWidth', 1.5);
y = plot(yy, 'LineWidth', 1.5);

flashOn = ddpiTime(vt{i}.timeFlashON);
flashOff = ddpiTime(vt{i}.timeFlashOFF);
targetOn = ddpiTime(vt{i}.timeTargetON);
targetOff = ddpiTime(vt{i}.timeTargetOFF);
respOn = ddpiTime(vt{i}.timeRespCueOFF - 500);
respOff = ddpiTime(vt{i}.timeRespCueOFF);

flash = fill([flashOn flashOn flashOff flashOff], ...
    [yLimit(1) (yLimit(2) - 0.5) (yLimit(2) - 0.5) yLimit(1)], ...
    [1.0, 0.2, 0.4], 'EdgeColor', 'none', 'FaceAlpha', 0.5, ...
    'Clipping', 'On');
target = fill([targetOn targetOn targetOff targetOff], ...
    [yLimit(1) (yLimit(2) - 0.5) (yLimit(2) - 0.5) yLimit(1)], ...
    [0.4, 0.6, 1.0], 'EdgeColor', 'none', 'FaceAlpha', 0.5, ...
    'Clipping', 'On');
respCue = fill([respOn respOn respOff respOff], ...
    [yLimit(1) (yLimit(2) - 0.5) (yLimit(2) - 0.5) yLimit(1)], ...
    [0.2, 1.0, 0.6], 'EdgeColor', 'none', 'FaceAlpha', 0.5, ...
    'Clipping', 'On');

legend([x y flash target respCue], 'x', 'y', 'flash', 'target', 'respCue', 'location', 'best')
xlabel('Sample')
ylim(yLimit);
ylabel('Position (arcmin)')
set(gca, 'linewidth', 2, 'fontsize', 20, 'Layer','top')
% kill = input('Discard this trial? 0 - No, 1 - Yes ');
input ''
cla

%saveas(gca, './IndvFigs/EyeTrace2.png');