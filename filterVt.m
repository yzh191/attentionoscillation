function [mat, out] = filterVt(trial, t1, t2, badEM)
mat = [];
for i = 1:length(badEM)
    if strcmp(badEM{i}, 'noResp')
        % check for no response
        if (trial.correct == 3)
            mat = [mat 1];
        else
            mat = [mat 0];
        end
    elseif strcmp(badEM{i}, 'gazeOff')
        if sum(abs(trial.x.position(ddpiTime(t1):ddpiTime(t2)) + trial.xOffset * trial.pxAngle) > 30) > 0 || ...
                sum(abs(trial.y.position(ddpiTime(t1):ddpiTime(t2)) + trial.yOffset * trial.pxAngle) > 30) > 0
            mat = [mat 1];
        else
            mat = [mat 0];
        end
    elseif strcmp(badEM{i}, 'rtInvalid')
        if trial.rt < 100 || trial.rt > 1000
            mat = [mat 1];
        else
            mat = [mat 0];
        end
    else
        if isBetween(t1, trial.(badEM{i}).start, t2)
            mat = [mat 1];
        else
            mat = [mat 0];
        end
    end
end
out = sum(mat);
