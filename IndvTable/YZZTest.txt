\begin{table}[H]                                          
\centering                                                
\begin{tabular}{|c|c|c|c|}                                
\hline                                                    
 & Cong. vs Incong. & Cong. vs Neut. & Incong. vs Neut. \\
\hline                                                    
LowSOA100 & 0.94 & 0.92 & 0.91 \\                         
\hline                                                    
LowSOA500 & 0.32 & 0.95 & 0.46 \\                         
\hline                                                    
HighSOA100 & 0.96 & 0.99 & 0.89 \\                        
\hline                                                    
HighSOA500 & 0.42 & 0.95 & 0.32 \\                        
\hline                                                    
\end{tabular}                                             
\caption{YZ z test}                                       
\label{table:YZZTest}                                     
\end{table}                                               
