# Attention Oscillation Experiment
## FOLDER AttnOscillationExp: contain experiment code for mk1
### Instructions for experimenter
1. set screen distance to 2 meters
2. nvidia display: brightness - 50%, contrast - 50%, all channels - 1
3. check params folder: subject name, stimulus frequency and contrast
### Instructions for subject
1. during reclibration trials, press X to precede to next trial
2. fixate at the central box during trial, press L1 or R2 to respond to the target
### Stimuli settings
* target size: 19 x 19 pixel (10' x 10')
* target frequency: 1 - 1 cycle (6 cpd), 2 - 3 cycles (18 cpd)
* target contrast

|Code|Contrast Grating (white --> black)|HighFreq Gabor|LowFreq Gabor|
|:----:|:----:|:----:|:----:|
|0|0 --> 1|NA|NA|
|1|0.1 --> 0.9|1|1|
|2|0.2 --> 0.8|0.8|0.8|
|3|0.3 --> 0.7|0.6|0.6|
|4|0.4 --> 0.6|0.5|0.4|
|5|0.45 --> 0.55|0.4|0.2|
|6|0.46 --> 0.54|0.3|0.1|
|7|0.47 --> 0.53|0.2|0.05|
|8|0.48 --> 0.52|0.1|NA|


