Step-by-step instructions

PRELIMINARY ADJUSTEMENTS
1. Turn on the rigth eyetracker ~20 minutes before starting the experiment
2. Place the monitor (ASUS ROG 278 labeled big ASUS on  -- see white label at the top of the monitor --- sitting on the light brown table)
   in the correct position.
3. Confirm that the monitor is at the correct location using the laser meter, the distance should be approximately 1550 mm from the 
   eye of the observer. (push the small table all the way to the back next to the small CRT table)
4. Make sure the monitor is centered and does not have any tilt. Use the leveler for checking the tilt and align the wheels of the table on the blue strip on the
floor.
5. Turn off any other monitor and turn on the experiment pc (pwd Qebgcsft2010)
6. Click on the Nvidia control panel icon at the bottom of the monitor on the icon tray (small green and black icon with a spiral)
7. Once the Nvidia panel opens, select the option on the left "Set up multiple display" and make sure that the two selected monitors in the 
   panel "Select the dysplay you want to use" are - Ancor Communication Inc VS248 and Ancor Communications Inc ROG PG278. 
8. Make sure the Gamma correction is set at 1. In the Nvidia panel select "Adjust desktop color settings" 
   Select "Use NVIDIA settings" and then select "Gamma" and "AllChannels" at the top and The number next to gamma should be set equal to 1. Select Apply.
9. In the NVIDIA control panel go to "Change Resolution" and set the resolution of the display at 1920 x 1080 select apply
10. Using the controls on the back of the ROG monitor make sure that the brightness and contrast of the display are set at 0
12. Open the folder: D:\Martina\FovealAttention3
12. Open the file: FovealAttention3.vcxproj
13. Click on build on the top left of the screen and select rebuild solution
14. If the console at the bottom does not report any error you can proceed
15. Hit F5 to run the experiment (make sure the DSP is on)

SETTING UP A NEW SUBJECT
1. make a copy of the folder "D:\Martina\FovealAttention3\Data\SubjectName"
2. Change the name of the copied folder with the subject name
3. open the new folder and change the progress txt file from "SubjectNameProgress.txt" and put the name of the subject in place of SubjectName
4. Open the params.cfg file (D:\Martina\FovealAttention3\Params.cfg) 
5. In the params file change the name of the subject and the other relevant variables. 
NOTE: if the subject has never been in the experiment before give clear instructions and set the following parameters to make the task really easy:
Rgb_value = 0
CueTime = 1000
TargetTime = 1000
Unstab = 1
Fovea = 1
CueTargetTime = 750

Monitor the performance of the subject and make sure that in the Neutral trials it sits at about 75% or higher.
Once the subject understands the task set the following variables at:
CueTime= 40 
TargetTime = 100
CueTargetTime = 60
Unstab = 0

Make sure the subject understand the recalibration procedure and is careful in adjusting the offset. The subject needs some time to understand
the recalibration procedure and perform it carefully.
Make sure that the subject mainteins the gaze stable and does not chase with the eyes the stimulus when it is presented under retinal 
stabilization. 

Once the subject is confortable you can start increasing the Rgb_value around 100 until you find a level when the subject performance in the neutral trials
is about 75% correct.

NOTE: Data are saved in the Subject folder but once the experiment session is finished you should place them in a subfolder with a meaningful label based on the
condition you run

--------------------------------------------
ATTENTION:

when changing screen setting and viewing distance make sure to change the following parameters in the Monitor Configuration file:

Distance = 1550

# Width of the screen (in mm)
Width = 600

# Height of the screen (in mm)
Height = 335

# Initial refresh rate and resolution of the welcome screen
H-Resolution = 1920 
V-Resolution = 1080
Refresh-Rate = 144






