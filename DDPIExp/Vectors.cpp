#include "stdafx.h"
#include "Vectors.h"
#include <iostream>
#include "emil/emil.h"
#include <vector>
#include <stdlib.h>

using namespace std;





/////////////////////////////////////////////////////////////////////////////
// Load a vector of strings
stringvector_t loadStringVector(std::string FileName)
{
	stringvector_t Vector;
	char Value[255];
	
	FILE* f = fopen(FileName.c_str(), "r");
	if (!f)
		throw xFile(xFile::ERR_OPEN, "loadStringVector", FileName);
	
	while (fscanf(f, "%s", &Value) > 0) {
		Vector.push_back(std::string(Value));
	}

	fclose(f);

	return stringvector_t(Vector);
}

/////////////////////////////////////////////////////////////////////////////
//