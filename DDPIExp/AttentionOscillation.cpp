
// Template.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include <emil-console\CWinEMIL.hpp>
#include <emil-console\CEMILConsole.hpp>
#include "AttentionOscillation.h"
#include "myManualCalibrator2.h"
#include "myAutoCalibration.h"
#include "ExperimentBody.h"
//#include <ExpDDPICalibration.hpp> 

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTemplateApp

BEGIN_MESSAGE_MAP(CAttentionOscillation, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTemplateApp construction

CAttentionOscillation::CAttentionOscillation()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	m_paramsFile.addVariable(CFG_SUBJECT, std::string(""));
	m_paramsFile.addVariable(CFG_DATA_PATH, std::string(""));
	m_paramsFile.addVariable(CFG_GABOR, 1);
	m_paramsFile.addVariable(CFG_CUE_ONLY, 1);
	m_paramsFile.addVariable(CFG_TARGET_FREQ, 1);
	m_paramsFile.addVariable(CFG_TARGET_ECC, 5.0f);
	m_paramsFile.addVariable(CFG_BOX_DIST, 5.0f);
	m_paramsFile.addVariable(CFG_PXANGLE, 5.0f);
	m_paramsFile.addVariable(CFG_VF, 1);
	m_paramsFile.addVariable(CFG_RGB, 1);
	m_paramsFile.addVariable(CFG_STEP, 1);
	m_paramsFile.addVariable(CFG_FIX_TIME, 1);
	m_paramsFile.addVariable(CFG_FLASH_TIME, 1);
	m_paramsFile.addVariable(CFG_START_SOA, 1);
	m_paramsFile.addVariable(CFG_END_SOA, 1);
	m_paramsFile.addVariable(CFG_TARGET_CUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_RESP_CUE_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_HOLD_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_TARGET_TIME, 5.0f);
	m_paramsFile.addVariable(CFG_DEBUG, 1);
	m_paramsFile.addVariable(CFG_FOVEA, 1);
	m_paramsFile.addVariable(CFG_FIX_SIZE, 1);
	m_paramsFile.addVariable(CFG_X_RES, 1024);
	m_paramsFile.addVariable(CFG_Y_RES, 768);
	m_paramsFile.addVariable(CFG_REFRESH_RATE, 150);
	m_paramsFile.addVariable(CFG_SCREEN_DIST, 150);
}


// The one and only CTemplateApp object

CAttentionOscillation theApp;


// CTemplateApp initialization

BOOL CAttentionOscillation::InitInstance()
{
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	CWinApp::InitInstance();
	AfxEnableControlContainer();
	CShellManager *pShellManager = new CShellManager;
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
	SetRegistryKey(_T("EyeRIS @ APLab"));

	// EyeRIS ------------------------------------------------------------------
	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	CWinEMIL::Instance()->initialize("./MonitorConfiguration/emil-library-monocular-singleROG.cfg");
	// Put your code here ======================================================
	
	
	// Load the specified configuration file
	m_paramsFile.loadFile("params.cfg");

	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	//CWinEMIL::Instance()->initialize("C:/EyeRIS/system/config/emil-library.cfg");

	string DataFolder = m_paramsFile.getString(CFG_DATA_PATH);
	string SubjectName= m_paramsFile.getString(CFG_SUBJECT);

	std::string DestinationDir = m_paramsFile.getDirectory(CFG_DATA_PATH) +
		m_paramsFile.getString(CFG_SUBJECT);


	int HResolution = m_paramsFile.getInteger(CFG_X_RES);
	int VResolution = m_paramsFile.getInteger(CFG_Y_RES);
	int RefreshRate = m_paramsFile.getInteger(CFG_REFRESH_RATE);
	int screenDist = m_paramsFile.getInteger(CFG_SCREEN_DIST);

	CEnvVariables::Instance()->addVariable(CFG_SCREEN_DIST, screenDist);

	//int post_time = m_paramsFile.getInteger(CFG_POST_CUE_TIME);

	// to set the initial target window
	// bool badal = false;

	/*if (m_paramsFile.getInteger(CFG_DEBUG) == 0) {
		ExpDDPICalibration* pexp2 = new ExpDDPICalibration(HResolution, VResolution, RefreshRate);
		pexp2->setGridDotSize(25);
		pexp2->setBackground(EIS_RGB(127, 127, 127));
		pexp2->setGridColor(EIS_RGB(0, 0, 0));
		CWinEMIL::Instance()->addExperiment(pexp2);

		ExAutoCalibrator2* autoCalibrator = new ExAutoCalibrator2(HResolution, VResolution, RefreshRate);
		autoCalibrator->setOutputDir(DestinationDir + "/calibration");
		autoCalibrator->setImageReversed(badal, badal);
		autoCalibrator->setGridPointSize(40);
		autoCalibrator->setBackgroundColor(127, 127, 127);
		CWinEMIL::Instance()->addExperiment(autoCalibrator);

		ExManualCalibrator2* manualCalibrator = new ExManualCalibrator2(HResolution, VResolution, RefreshRate);
		manualCalibrator->setOutputDir(DestinationDir + "/calibration");
		manualCalibrator->setImageReversed(badal, badal);
		manualCalibrator->setGridPointSize(40);
		manualCalibrator->setBackgroundColor(127, 127, 127);
		CWinEMIL::Instance()->addExperiment(manualCalibrator);
	}*/

	ExperimentBody* pexp = new ExperimentBody(HResolution, VResolution, RefreshRate, &m_paramsFile);
	// pexp->setImageReversed(badal, badal);
	CWinEMIL::Instance()->addExperiment(pexp);
	
	// =========================================================================
	//CTemplateDlg dlg;
	CEMILConsole dlg("./MonitorConfiguration/emil-console-monocular-singleROG.cfg");
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	// Destroy the the library
	CWinEMIL::Destroy();
	// EyeRIS ------------------------------------------------------------------
	
	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

