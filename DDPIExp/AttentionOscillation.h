
// Template.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include <emil/emil.hpp>

const std::string CFG_SUBJECT = "subject";
const std::string CFG_RGB = "RGB";
const std::string CFG_TARGET_FREQ = "TargetFreq";
const std::string CFG_GABOR = "Gabor";
const std::string CFG_STEP = "step";
const std::string CFG_CUE_ONLY = "CueOnly";
const std::string CFG_TARGET_ECC = "TargetEcc";
const std::string CFG_BOX_DIST = "BoxDist";
const std::string CFG_PXANGLE = "PxAngle";
const std::string CFG_VF = "VF";
const std::string CFG_DATA_PATH = "DataPath";
const std::string CFG_FIX_TIME = "FixTime";
const std::string CFG_FLASH_TIME = "FlashTime";
const std::string CFG_RESP_CUE_TIME = "ReponseCueTime";
const std::string CFG_START_SOA = "StartSOA";
const std::string CFG_END_SOA = "EndSOA";
const std::string CFG_TARGET_CUE_TIME = "TargetCueTime";
const std::string CFG_HOLD_TIME = "HoldTime";
const std::string CFG_TARGET_TIME = "TargetTime";
const std::string CFG_DEBUG = "debug";
const std::string CFG_FOVEA = "Fovea";
const std::string CFG_FIX_SIZE = "FixSize";
const std::string CFG_X_RES = "X_res";
const std::string CFG_Y_RES = "Y_res";
const std::string CFG_REFRESH_RATE = "RefreshRate";
const std::string CFG_SCREEN_DIST= "ScreenDistance";

class CAttentionOscillation : public CWinApp
{
public:
	CAttentionOscillation();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	
	// Configuration file
	CCfgFile m_paramsFile;
};

extern CAttentionOscillation theApp;
