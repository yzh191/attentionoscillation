#ifndef _EXPERIMENTBODY_H
#define _EXPERIMENTBODY_H

#include "emil/emil.hpp"
#include <fstream>
#include <string>


using namespace std;

class ExperimentBody: public CExperiment
{
public:
	ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params);

	/// Standard event handlers
	void initialize();
	void finalize();

	void eventRender(unsigned int FrameCount, CEOSData* Samples);
	void eventKeyboard(unsigned char key, int x, int y);
	void eventJoypad();

private:
	enum TRIAL_TYPE { CONGRUENT = 1, INCONGRUENT = 2, NEUTRAL = 3 };
	enum LOC { NONE, TOP, BOTTOM };
	enum VISUAL_FIELD { BOTH, LEFT, RIGHT };
	enum TARGET_TILT { L , R };
	//enum TARGET_SHAPE { GRATING = 1, GABOR = 2 };

	uint8_t trialType;
	uint8_t flashLoc;
	uint8_t VF;
	uint8_t respLoc;
	uint8_t t1Tilt;
	uint8_t t2Tilt;

	bool isGabor;
	std::string fImage1;
	std::string fImage2;

	// Current experiment state
	enum STATE {
		STATE_LOADING,
		STATE_TESTCALIBRATION,
		STATE_FIXATION,
		STATE_FLASH,
		STATE_TARGET,
		STATE_PAUSE,
		STATE_RESPONSE_CUE,
		STATE_RESPONSE,
	};
	STATE m_state;
	//void gotoLoading();
	void gotoFixation();
    void saveData();
	int ExperimentBody::randIntv(int first, int last);
	int ExperimentBody::findCeil(std::vector<int> arr, int r, int l, int h);
	int ExperimentBody::randList(int arr[], int freq[], int n);

	// Configuration file
	CCfgFile* m_paramsFile;
	CImagePlane* m_respcue;
	CImagePlane* m_box1;
	CImagePlane* m_box2;
	CImagePlane* m_arcs;
	CImagePlane* m_target1;
	CImagePlane* m_target2;
	CImagePlane* m_flash1;
	CImagePlane* m_flash2;

	// Stimuli for the test calibration
	CSolidPlane* m_blackbox;
	CSolidPlane* m_whitebox;
	CSolidPlane* m_fixation;

	int t1Phase;
	int t2Phase;
	int targetFreq;
	int RGB;
	bool debug;
	bool hasCalibrated;
	bool testCalibration;
	bool waitResponse;
	// start here
	int nTrial;
	float corrCong;
	float corrIncong;
	float corrNeutral;
	float nCong;
	float nIncong;
	float nNeutral;
	float respTime;
	int step;
	float respCueTime;
	float flashTime;
	float ISI;
	int correctTarget;
	int startSOA;
	int endSOA;
	int SOA;
	float timeFlashON;
	float timeFlashOFF;
	float timeTargetON;
	float timeTargetOFF;
	float timeRespCueOFF;
	float timeFixON;
	float timeFixOFF;
	float targetEcc;
	float boxSize;
	float flashSize;
	float flashDist;
	float nRecal;
	float diagEcc;
	float pauseTime;
	int screenDist;
	float pxAngle;
	float pxAngle0;
	float boxDist;
	float yDist;
	float xDist;
	float gazeDist;
	int ScreenDistance;
	int fixSize;
	int cueOnly;

	// timers
	CTimer m_pause;
	CTimer m_timer;
	CTimer m_timerCheck;
	CTimer m_timerExp;
	CTimer m_timerfixation;
	CTimer m_timerflash; 
	CTimer m_timerresponsecue;
	CTimer m_timerisi;
	CTimer m_timerhold;
	CTimer m_timertarget;
	CTimer m_timerpause;

	
	float stop_time; //
	float ResponseTime;
	int WAIT_RESPONSE;
	float Correct;
	int Response;
	float X;
	float Y;
	float xStab;
	float yStab;
	float xPos;
	float yPos;
	float xShift;
	float yShift;
	
	bool SHOW_TARGET;

};
#endif // _EXPERIMENTBODY_H
