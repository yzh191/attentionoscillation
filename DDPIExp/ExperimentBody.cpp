#include "stdafx.h"
#include "ExperimentBody.h"
#include "AttentionOscillation.h"
#include <iostream>
#include <random>
#include <math.h>

const float PI = 3.1415926353f;

///////// a randomize function to generate SOA from uniform distribution
int ExperimentBody::randIntv(int first, int last)
{
	std::random_device rd; // obtain a random number from hardware
	std::mt19937 gen(rd()); // seed the generator
	std::uniform_int_distribution<> distr(first, last); // define the range
	return distr(gen); // return the integer
}


// //////////////////Utility function to find ceiling of r in arr[l..h]
int ExperimentBody::findCeil(std::vector<int> arr, int r, int l, int h)
{
	int mid;
	while (l < h)
	{
		mid = l + ((h - l) >> 1);  // Same as mid = (l+h)/2
		(r > arr[mid]) ? (l = mid + 1) : (h = mid);
	}
	return (arr[l] >= r) ? l : -1;
}
// The main function that returns a random number from arr[] according to
// distribution array defined by freq[]. n is size of arrays.
int ExperimentBody::randList(int arr[], int freq[], int n)
{
	srand(time(NULL));
	// Create and fill prefix array
	std::vector<int> prefix(n, 0);
	int i;
	prefix[0] = freq[0];
	for (i = 1; i < n; ++i)
		prefix[i] = prefix[i - 1] + freq[i];

	// prefix[n-1] is sum of all frequencies. Generate a random number
	// with value from 1 to this sum
	int r = (rand() % prefix[n - 1]) + 1;

	// Find index of ceiling of r in prefix arrat
	int indexc = findCeil(prefix, r, 0, n - 1);
	return arr[indexc];
}

///////////////////////////////////////////////////////////////////////////////////
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) :
	CExperiment(pxWidth, pxHeight, RefreshRate)
	
{
	setExperimentName("AttentionOscillation");
	m_paramsFile = Params;
	CMath::srand(time(NULL));
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::initialize()
{		
	CStabilizer::Instance()->enableSlowStabilization(false);
	
	// import and set conditions
	debug = m_paramsFile->getInteger(CFG_DEBUG);
	VF = m_paramsFile->getInteger(CFG_VF);
	step = m_paramsFile->getInteger(CFG_STEP);
	startSOA = m_paramsFile->getInteger(CFG_START_SOA);
	endSOA = m_paramsFile->getInteger(CFG_END_SOA);
	targetEcc = m_paramsFile->getFloat(CFG_TARGET_ECC);
	cueOnly = m_paramsFile->getInteger(CFG_CUE_ONLY);
	boxDist = m_paramsFile->getFloat(CFG_BOX_DIST);
	RGB = m_paramsFile->getInteger(CFG_RGB);
	fixSize = m_paramsFile->getInteger(CFG_FIX_SIZE);
	targetFreq = m_paramsFile->getInteger(CFG_TARGET_FREQ);
	isGabor = m_paramsFile->getInteger(CFG_GABOR);

	//pxAngle0 = CConverter::Instance()->px2a(1.f);
	//pxAngle = 1.0 * pxAngle0;
	screenDist = m_paramsFile->getInteger(CFG_SCREEN_DIST);

	// convert from arcmin to px
	pxAngle = 0.5305; // distance of 2 meters

	boxSize = 25;
	CEnvironment::Instance()->outputMessage("Target Size: %.2f px", boxSize);

	flashSize = 6 / pxAngle;
	flashDist = boxSize / 2 + flashSize / 2 + 6;
	targetEcc = targetEcc / pxAngle;
	yDist = (boxDist / 2) / pxAngle;
	xDist = sqrt(pow(targetEcc, 2) - pow(yDist, 2));
	diagEcc = targetEcc / sqrt(2); // calculate diagnal boxes distance of boxes from center

	m_fixation = addObject(new CSolidPlane(0,0,0));
	m_fixation->pxSetPosition(0,0);
	m_fixation->pxSetSize(fixSize / pxAngle, fixSize / pxAngle);
	m_fixation->hide();

	// add boxes
	//m_box1 = addObject(new CImagePlane("images/square.tga"));
	//m_box1->enableTrasparency(true);
	//m_box1->replaceGray(0, 75); //75
	//m_box1->pxSetSize(27, 27);
	//m_box1->pxSetPosition(0, 0);
	//m_box1->hide();

	//m_box2 = addObject(new CImagePlane("images/square.tga"));
	//m_box2->enableTrasparency(true);
	//m_box2->replaceGray(0, 75); //75
	//m_box2->pxSetSize(27, 27);
	//m_box2->pxSetPosition(0, 0);
	//m_box2->hide();



	//m_flash1 = addObject(new CImagePlane("images/flash.tga"));
	//m_flash1->pxSetSize(29, 29);
	//m_flash1->pxSetPosition(0, 0);
	//m_flash1->enableTrasparency(true);
	//m_flash1->replaceGray(0, 75); //75
	//m_flash1->hide();

	//m_flash2 = addObject(new CImagePlane("images/flash.tga"));
	//m_flash2->pxSetSize(29, 29);
	//m_flash2->pxSetPosition(0, 0);
	//m_flash2->enableTrasparency(true);
	//m_flash2->replaceGray(0, 75); //75
	//m_flash2->hide();

	m_box1 = addObject(new CImagePlane("images/PlaceHolder.tga"));
	m_box1->enableTrasparency(true);
	m_box1->pxSetSize(34, 34); //34
	m_box1->pxSetPosition(0, 0);
	m_box1->hide();

	m_box2 = addObject(new CImagePlane("images/PlaceHolder.tga"));
	m_box2->enableTrasparency(true);
	m_box2->pxSetSize(34, 34);
	m_box2->pxSetPosition(0, 0);
	m_box2->hide();

	m_flash1 = addObject(new CImagePlane("images/flash.tga"));
	m_flash1->pxSetSize(40, 40);
	m_flash1->pxSetPosition(0, 0);
	m_flash1->enableTrasparency(true);
	m_flash1->hide();

	m_flash2 = addObject(new CImagePlane("images/flash.tga"));
	m_flash2->pxSetSize(40, 40);
	m_flash2->pxSetPosition(0, 0);
	m_flash2->enableTrasparency(true);
	m_flash2->hide();


	//set conditions
	m_respcue = addObject(new CImagePlane("images/RespCue.tga"));
	m_respcue->pxSetSize(25, 25);
	
	m_respcue->enableTrasparency(true);
	m_respcue->pxSetPosition(0, 0);
	m_respcue->hide();


	// add all display images
	m_arcs = addObject(new CImagePlane("images/arcs.tga"));
	m_arcs->enableTrasparency(true);
	m_arcs->pxSetSize(200, 200);
	m_arcs->hide();

	CEnvironment::Instance()->outputMessage("Target: frequency - %i, contrast - %i", targetFreq, RGB);

	// set the pixel increment for the test calibration procedure during the exp
	hasCalibrated = false;
	xShift = 0;
	yShift = 0;
	xPos = 0;
	yPos = 0;
	nTrial = 1;
	nRecal = 0;
	nCong = 0;
	nIncong = 0;
	nNeutral = 0;
    corrCong = 0;
    corrIncong = 0;
    corrNeutral = 0;

	// set testCalibration = 1 so that the experiment will start with 
    // a recalibration trial
	testCalibration = !debug;
	stop_time = 1000;

	// boxes for the recalibration trials	
	m_whitebox = addObject(new CSolidPlane(255, 255, 255));
	m_whitebox->pxSetSize(13,13);
	m_blackbox = addObject(new CSolidPlane(0, 0, 0));
	m_blackbox->pxSetSize(13,13);

	enable(CExperiment::EIS_PHOTOCELL);
	disable(CExperiment::EIS_NOTRACK_ICON);
	enable(CExperiment::EIS_STAT1);
	
	pauseTime = 100; //time between target & response cue
	respCueTime = m_paramsFile->getFloat(CFG_RESP_CUE_TIME);
	
	hideAllObjects();
	m_state = STATE_LOADING;
	m_timer.start(1000);

	waitResponse = true;
	
	/* Seed the random-number generator with current time so that
    * the numbers will be different every time we run.*/
    srand( (unsigned)time( NULL ) );

}
///////////////////////////////////////////////////////////////////////////////////
// write the progress file
void ExperimentBody::finalize()
{
	// write progress
	//check to see that some experiments have been conducted
	if(nTrial > 0){
		char LocalDate[1024];
		time_t t = time(NULL);
		strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));
		ostringstream fstr1;
		fstr1<< "Data\\"<<m_paramsFile->getString(CFG_SUBJECT)<<"\\"<<m_paramsFile->getString(CFG_SUBJECT) <<"Progress.txt";
		ofstream out1(fstr1.str().c_str(), ios::app);
		if(!out1.is_open()){
			CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "Session data file (%sProgress.txt) could not be opened.  Please check that the file exists.", m_paramsFile->getString(CFG_SUBJECT));
			declareFinished();
		}
	    // keep track of the last trial played in the list and start from there in the next session
		out1<< "Recorded: " << LocalDate <<endl;
		out1 << "Total trials: " << nTrial << endl;
		out1 << "Congruent: " << nCong << endl;
		out1 << "Incongruent: " << nIncong << endl;
		out1 << "Neutral: " << nNeutral << endl;
	}

}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples)
{
	
	//need arcs only for stabilized foveal condition
	m_arcs->show();
	CStabilizer::Instance()->
		stabilize(Samples, X, Y);
	if (debug){
		X = 0; //0
		Y = 0; //0
	}
	storeTrialStream(0, X);
	storeTrialStream(1, Y);	

	xStab = X;
	yStab = Y;

	// add the offset
	X = X + xShift;
	Y = Y + yShift;
        // Flash Location & Response Location based on Trial Type
    switch (VF) {
		case BOTH: // symmetrical 
			m_box1->pxSetPosition(X - targetEcc, Y);
			m_box2->pxSetPosition(X + targetEcc, Y);
			m_flash1->pxSetPosition(X - targetEcc, Y);
			m_flash2->pxSetPosition(X + targetEcc, Y);
			break;
        case LEFT: //left
            m_flash1->pxSetPosition(X - xDist - flashDist, Y + yDist + flashDist);
            m_flash2->pxSetPosition(X - xDist - flashDist, Y - yDist - flashDist);
            break;
        case RIGHT: //right
            m_flash1->pxSetPosition(X + xDist + flashDist, Y + yDist + flashDist);
            m_flash2->pxSetPosition(X + xDist + flashDist, Y - yDist - flashDist);
            break;
        default:
            break;
    }

	float x;
	float y;
	static bool gate;

	switch (m_state) {
		case STATE_LOADING:

			COGLEngine::Instance()->clearScreen();
			glColor3d(255, 255, 255);

			// Set the background color for the experiment
			COGLEngine::Instance()->setBackgroundColor(127, 127, 127);

			// Copy the parameter file into the subject directory
			if (m_timer.isExpired()){
				char LocalDate[1024];
				time_t t = time(NULL);
				strftime(LocalDate, 1024, "%Y-%m-%d-%H-%M-%S", localtime(&t));
				ostringstream DestinationFileName;
				DestinationFileName << m_paramsFile->getDirectory(CFG_DATA_PATH) <<
					m_paramsFile->getString(CFG_SUBJECT) << "/" << m_paramsFile->getString(CFG_SUBJECT) <<
					"-" << LocalDate << "-params.cfg";;
				gotoFixation();
				gate = true;
			}
			break;
		case STATE_TESTCALIBRATION:
			if (!m_timerCheck.isExpired()){
				CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
			}
			else{
				if (!hasCalibrated){
					m_whitebox->pxSetPosition(0, 0);
					m_whitebox->show();
					CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
					m_blackbox->pxSetPosition(x + xShift + xPos, y + yShift + yPos);
					m_blackbox->show();

				}
				else{
					testCalibration = false;
					xShift = xPos + xShift;
					yShift = yPos + yShift;
					m_blackbox->hide();
					m_whitebox->hide();
					endTrial();
					gotoFixation();
				}
			}
			break;
		case STATE_FIXATION:
			storeTrialStream(0, xStab);
			storeTrialStream(1, yStab);
			m_fixation->pxSetPosition(X, Y); 
			m_fixation->show(); 
			//m_box1->show();
			//m_box2->show();
			// check if the subject is looking at the center
			//use stabilized positions acquired at the beginning of the render cycle 
			gazeDist = sqrt((pow(0 - (X), 2) + pow(0 - (Y), 2)));

			// Code to run at first render cycle
			if (gate){
				if (gazeDist < 500 || debug) {
					CStabilizer::Instance()->resetFilter(Samples->x1, Samples->y1);
					// time for fixation ON
					timeFixON = m_timerExp.getTime();
					// fixation should stay on for one sec or as far as the gaze is stabilized
					m_timerfixation.start(m_paramsFile->getFloat(CFG_FIX_TIME));
					if (debug) {
						X = 0;
						Y = 0;
					}
				}
			gate = false;
			}
			if (m_timerfixation.isExpired()){			
				//randomize trials
				trialType = (cueOnly == 1) ? randIntv(1, 2) : randIntv(1, 3);
				// Flash Location & Response Location based on Trial Type
				switch (trialType) {
					case CONGRUENT:
						flashLoc = randIntv(1, 2);
						respLoc = flashLoc;
						nCong++;
						CEnvironment::Instance()->outputMessage("Congruent Trial %.1f", nCong);
						break;
					case INCONGRUENT:
						flashLoc = randIntv(1, 2);
						respLoc = flashLoc == 1 ? 2 : 1;
						nIncong++;
						CEnvironment::Instance()->outputMessage("Incongruent Trial %.1f", nIncong);
						break;
					case NEUTRAL:
						flashLoc = 0;
						respLoc = randIntv(1, 2);
						nNeutral++;
						CEnvironment::Instance()->outputMessage("Neutral Trial %.1f", nNeutral);
						break;
					default:
						break;
				}
				m_state = STATE_FLASH;
			}
			break;
		case STATE_FLASH:
			//storeTrialStream(0, xStab);
			//storeTrialStream(1, yStab);

			if (debug){
				X = 0;
				Y = 0;
			}
			m_fixation->pxSetPosition(X, Y);
			m_fixation->show();
			moveToFront(m_fixation);
			// check if the subject is looking at the fixation and that there is a drift (no saccades or notrack)
			if (!gate) {
				if ((m_timerfixation.isExpired()) || (debug)) {
					timeFixOFF = m_timerExp.getTime();
					switch (flashLoc) {
					case NONE: //
						//m_flash1->hide();
						//m_flash2->hide();
						m_flash1->show();
						moveToFront(m_flash1);
						m_flash2->show();
						moveToFront(m_flash2);
						break;
					case TOP:
						m_flash1->show();
						moveToFront(m_flash1);
						m_flash2->hide();
						break;
					case BOTTOM:
						m_flash2->show();
						moveToFront(m_flash2);
						m_flash1->hide();
						break;
					default:
						break;
					}
					// time for cue presentation
					timeFlashON = m_timerExp.getTime();
					gate = true;
					flashTime = m_paramsFile->getFloat(CFG_FLASH_TIME);
					// set the time for the cue 
					m_timerflash.start(flashTime);
				}
			}
			if (gate) {
				if (!m_timerflash.isExpired()) {
					// continue to stabilize the cue
					if (debug) {
						X = 0;
						Y = 0;
					}
					else {
						switch (flashLoc) {
						case TOP:
							m_flash1->show();
							moveToFront(m_flash1);
							m_flash2->hide();
							break;
						case BOTTOM:
							m_flash2->show();
							moveToFront(m_flash2);
							m_flash1->hide();
							break;
						default:
							break;
						}
					}
				}
				else if (m_timerflash.isExpired()) {
					m_flash1->hide();
					m_flash2->hide();
					m_state = STATE_TARGET;
					// randomize SOA within a fixed interval from params
					SOA = randIntv(startSOA, endSOA); 
					// For now, randomize between 100 and 500, just the two SOAs
					//int SOAList[] = { 100, 500 };
					//int SOAFreq[] = { 50, 50 };
					//int n = sizeof(SOAList) / sizeof(SOAList[0]);
					//SOA = randList(SOAList, SOAFreq, n);
					ISI = SOA - flashTime; // SOA - flashTime = ISI
					m_timerisi.start(ISI);
					m_respcue->hide();
					gate = false;
					timeFlashOFF = m_timerExp.getTime();

					// print on console
					CEnvironment::Instance()->outputMessage("SOA: %i ms", SOA);
					CEnvironment::Instance()->outputMessage("ISI: %i ms", ISI);
					CEnvironment::Instance()->outputMessage("Flash Location | Resp. Location");
					CEnvironment::Instance()->outputMessage("          %i             |            %i            ", flashLoc, respLoc);
				}
			}

			break;

			// STATE_TARGET: present the target
			//              go to the next state and wait for subject response
		case STATE_TARGET:

			//storeTrialStream(0, xStab);
			//storeTrialStream(1, yStab);
			m_fixation->pxSetPosition(X, Y);
			m_fixation->show();
			m_box1->pxSetPosition(X - targetEcc, Y);
			m_box2->pxSetPosition(X + targetEcc, Y);
			if (debug){
				X = 0;
				Y = 0;
			}
			if (m_timerisi.isExpired() && (!gate)){
				// change t1Tilt orientation
				switch (t1Tilt) {
				case L:
					m_target1->degSetAngle(0);
					break;
				case R:
					m_target1->degSetAngle(90);
					break;
				default:
					break;
				}
				// change t2Tilt orientation
				switch (t2Tilt) {
				case L:
					m_target2->degSetAngle(0);
					break;
				case R:
					m_target2->degSetAngle(90);
					break;
				default:
					break;
				}
				moveToFront(m_target1);
				moveToFront(m_target2);

				timeTargetON = m_timerExp.getTime();
				// set target duration			
				m_timertarget.start(m_paramsFile->getFloat(CFG_TARGET_TIME));

				gate = true;
			}
			if ((gate) && (m_timertarget.isExpired())){ // go to the next state

				m_fixation->pxSetPosition(X, Y); 
				m_fixation->show();
				m_target1->hide();
				m_target2->hide();

				waitResponse = false;
				timeTargetOFF = m_timerExp.getTime();

				m_timerpause.start(pauseTime);  
				m_state = STATE_PAUSE;
			}

			else if ((gate) && (!m_timertarget.isExpired())){
				m_fixation->pxSetPosition(X, Y);
				m_fixation->show();

                switch (VF) {
					case BOTH:
						m_target1->pxSetPosition(X - targetEcc, Y);
						m_target2->pxSetPosition(X + targetEcc, Y);
						break;
                    case LEFT: //left
                        m_target1->pxSetPosition(X - xDist, Y + yDist);
                        m_target2->pxSetPosition(X - xDist, Y - yDist);
                        break;
                    case RIGHT: //right
                        m_target1->pxSetPosition(X + xDist, Y + yDist);
                        m_target2->pxSetPosition(X + xDist, Y - yDist);
                        break;
                    default:
                        break;
                }

				m_target1->show();
				m_target2->show();
				moveToFront(m_target1);
				moveToFront(m_target2);
			}

			//moveToFront(m_box1);
			//moveToFront(m_box2);

			break;

		case STATE_PAUSE: 
			m_respcue->hide();

			//storeTrialStream(0, xStab);
			//storeTrialStream(1, yStab);

			m_fixation->pxSetPosition(X, Y);
			m_fixation->show();
			if (debug){
				X = 0;
				Y = 0;
			}

			if (m_timerpause.isExpired()){
				m_timerresponsecue.start(respCueTime);
				m_state = STATE_RESPONSE_CUE;
			}

			break;

		case STATE_RESPONSE_CUE:

			//storeTrialStream(0, xStab);
			//storeTrialStream(1, yStab);
			// continue to stabilize the fixation marker;

			m_fixation->pxSetPosition(X, Y);
			m_fixation->show();
			if (debug){
				X = 0;
				Y = 0;
			}

			if (!m_timerresponsecue.isExpired()){
				switch (respLoc) {
				case TOP:
					correctTarget = t1Tilt;
					switch (VF) {
					case BOTH:
						m_respcue->degSetAngle(135);
						break;
					case LEFT: //left
						m_respcue->degSetAngle(115);
						break;
					case RIGHT: //right
						m_respcue->degSetAngle(-25);
						break;
					default:
						break;
					}
					break;
				case BOTTOM:
					correctTarget = t2Tilt;
					switch (VF) {
					case BOTH:
						m_respcue->degSetAngle(-45);
						break;
					case LEFT: //left
						m_respcue->degSetAngle(155);
						break;
					case RIGHT: //right
						m_respcue->degSetAngle(-65);
						break;
					default:
						break;
					}
				default:
					break;
				}
				m_respcue->pxSetPosition(X, Y);
				m_respcue->show();
				moveToFront(m_fixation); 
			}
			else{
				//add here the time response cue off
				m_respcue->hide();
				timeRespCueOFF = m_timerExp.getTime();
				m_timerhold.start(m_paramsFile->getFloat(CFG_HOLD_TIME));
				m_state = STATE_RESPONSE;
			}
			break;
			
		
		// STATE_RESPONSE: wait for subject response 
		case STATE_RESPONSE:

			m_flash1->hide();
			m_flash2->hide();

			//storeTrialStream(0, xStab);
			//storeTrialStream(1, yStab);
				
				if (!(m_timerhold.isExpired())){
				// continue to stabilize the fixation marker
					if (debug){
						X = 0;
						Y = 0;
					}
					m_fixation->pxSetPosition(X,Y);
					m_fixation->show();
				}

				if ((m_timerhold.isExpired()) && (!waitResponse)){
					// response is not given
					respTime = 0;	
					// hide all the objects 
					hideAllObjects();
					m_target1->hide();
					m_target2->hide();
					m_fixation->hide();
					m_respcue->hide();
					
					endTrial();
					saveData();	
				}
				else if (waitResponse){
					// hide all the objects 
					hideAllObjects();
					m_target1->hide();
					m_target2->hide();
					m_fixation->hide();
					endTrial();
					saveData();	
				}	

			break;
			}
	}		
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventJoypad()
{
	// activate the joypad only in the state calibration	

			if (m_state == STATE_TESTCALIBRATION) 
			{				

				if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) // moving the cursor up
				{
					yPos = yPos + step; //position of the cross
				}
			
				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) // moving the cursor down
				{	
					yPos = yPos - step;
				}
		
				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) // moving the cursor to the right
				{
					xPos = xPos + step;

				}

				else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) // moving the cursor to the left
				{
					xPos = xPos - step;
				
				}
				
				if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_X)) // finalize the response
				{
						hasCalibrated = true; // click the left botton to finalize the response

				}
		   }
			if (STATE_RESPONSE || STATE_RESPONSE_CUE){

				if ( (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) ||
					(CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) ){
					waitResponse = true;
					// get the time of the response here
					respTime =  m_timerExp.getTime();
					
					if (correctTarget == 0) {
						CEnvironment::Instance()->outputMessage("\nTarget: left");
					}
					else if(correctTarget == 1) {
						CEnvironment::Instance()->outputMessage("\nTarget: right");
					}
					// button press L1 - left
					if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) {
						Response = 0;
						CEnvironment::Instance()->outputMessage("Response: left");
                        if (correctTarget == 0){
                            Correct = 1;
                        }
                        else if (correctTarget == 1){
                            Correct = 0;
                        }
					}
					//button press R1 - right
					else if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
						Response = 1;
						CEnvironment::Instance()->outputMessage("Response: right");
                        if (correctTarget == 1){
                            Correct = 1;
                        }
                        else if (correctTarget == 0){
                            Correct = 0;
                        }
					}

				}

			}
}

///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::eventKeyboard(unsigned char key, int x, int y)
{
	// activate the joypad only in the state calibration	

	if (m_state == STATE_TESTCALIBRATION)
	{

		if (key == 'w' || key == 'W') // moving the cursor up
		{
			yPos = yPos + step; //position of the cross
		}

		else if (key == 's' || key == 'S') // moving the cursor down
		{
			yPos = yPos - step;
		}

		else if (key == 'd' || key == 'D') // moving the cursor to the right
		{
			xPos = xPos + step;

		}

		else if (key == 'a' || key == 'A') // moving the cursor to the left
		{
			xPos = xPos - step;

		}

		if (key == 'x' || key == 'X') // finalize the response
		{
			hasCalibrated = true; // click the left botton to finalize the response

		}
	}
	if (STATE_RESPONSE || STATE_RESPONSE_CUE) {

		if ((key == 'a' || key == 'A') ||
			(key == 'd' || key == 'D')) {
			waitResponse = true;
			// get the time of the response here
			respTime = m_timerExp.getTime();

			if (correctTarget == 0) {
				CEnvironment::Instance()->outputMessage("\nTarget: left");
			}
			else if(correctTarget == 1) {
				CEnvironment::Instance()->outputMessage("\nTarget: right");
			}

			if (key == 'a' || key == 'A') {
				Response = 0;
				CEnvironment::Instance()->outputMessage("Response: left");
                if (correctTarget == 0){
                    Correct = 1;
                }
                else if (correctTarget == 1){
                    Correct = 0;
                }
			}
			else if (key == 'd' || key == 'D') {
				Response = 1;
				CEnvironment::Instance()->outputMessage("Response: right");
                if (correctTarget == 1){
                    Correct = 1;
                }
                else if (correctTarget == 0){
                    Correct = 0;
                }
			}

		}

	}
}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::gotoFixation()
{
	//nTrial++;
    hideAllObjects();


    if (testCalibration){
        m_state = STATE_TESTCALIBRATION;
        hasCalibrated = false;
        m_timerCheck.start(500);
    }
    else{
		CEnvironment::Instance()->outputMessage("Trial Number: %i", (nTrial));
        m_state = STATE_FIXATION;
		// randomize target orientation between 0 and 1 (0 - left, 1 - right)
		t1Tilt = randIntv(0, 1); //1 and 2 are tilted left, 3 and 4 are tilted right, odd numbers are phase of 0
		t2Tilt = randIntv(0, 1);
		
		int phaseList[] = { 90, 270 };
		int phaseFreq[] = { 50, 50 };
		int n = sizeof(phaseList) / sizeof(phaseList[0]);
		t1Phase = randList(phaseList, phaseFreq, n);
		t2Phase = t1Phase; //randomizing phase


		CEnvironment::Instance()->outputMessage("T1 phase: %i, T2 phase: %i", t1Phase, t2Phase);
		// load image with selected frequency and RGB
		if (isGabor) {
			fImage1 = "images/gabor/" + std::to_string(targetFreq) + "F" + std::to_string(t1Phase) + "D" + std::to_string(RGB) + "A.tga";
			fImage2 = "images/gabor/" + std::to_string(targetFreq) + "F" + std::to_string(t2Phase) + "D" + std::to_string(RGB) + "A.tga";
		}
		else if (!isGabor) {
			fImage1 = "images/grating/" + std::to_string(targetFreq) + "F" + std::to_string(t1Phase) + "D" + std::to_string(RGB) + "C.tga";
			fImage2 = "images/grating/" + std::to_string(targetFreq) + "F" + std::to_string(t2Phase) + "D" + std::to_string(RGB) + "C.tga";
		}

		m_target1 = addObject(new CImagePlane(fImage1));
		m_target1->enableTrasparency(true);
		m_target1->pxSetSize(boxSize, boxSize);
		m_target1->hide();

		m_target2 = addObject(new CImagePlane(fImage2));
		m_target2->enableTrasparency(true);
		m_target2->pxSetSize(boxSize, boxSize);
		m_target2->hide();

        // if the response is not delivered the veriable correct is left at 3
        Correct = 3;
        Response = 3;
}

	// start the trial
	waitResponse = true;	
	startTrial();
	m_timerExp.start();
	m_timer.start(1000);

}
///////////////////////////////////////////////////////////////////////////////////
void ExperimentBody::saveData()
{
		if (respTime > 0)
	    // give confrimation of response
		Beep(600,400);

		// time of the response (locked to the start of the trial)
		storeTrialVariable("timeFlashON", timeFlashON);
		storeTrialVariable("timeFlashOFF", timeFlashOFF);
		storeTrialVariable("timeFixON", timeFixON); 
		storeTrialVariable("timeFixOFF", timeFixOFF); 
		storeTrialVariable("timeTargetON", timeTargetON);
		storeTrialVariable("timeTargetOFF", timeTargetOFF);
		storeTrialVariable("timeRespCueOFF", timeRespCueOFF); //added pause to this
		storeTrialVariable("correct", Correct);
		storeTrialVariable("reactionTime", respTime);
		storeTrialVariable("targetFrequency", targetFreq);
		storeTrialVariable("isGabor", isGabor);

		//0 is left and 1 is right
		storeTrialVariable("t1Tilt", t1Tilt);
		storeTrialVariable("t2Tilt", t2Tilt);
		storeTrialVariable("t1Phase", t1Phase);
		storeTrialVariable("t2Phase", t2Phase);
		storeTrialVariable("response", Response);
		storeTrialVariable("pxAngle", pxAngle);

        
		if (Correct == 1)
			CEnvironment::Instance()->outputMessage("Correct");
		if (Correct == 0)
			CEnvironment::Instance()->outputMessage("Wrong");
	
    switch (trialType) {
        case CONGRUENT:
            if ( Correct == 0 || Correct == 1 )
                corrCong = corrCong + Correct;
            CEnvironment::Instance()->outputMessage("\nCong. Accuracy: %f ", corrCong/nCong);
            break;
        case INCONGRUENT:
            if (Correct == 0 || Correct == 1)
                corrIncong = corrIncong + Correct;
            CEnvironment::Instance()->outputMessage("\nIncong. Accuracy: %f ", corrIncong/nIncong);
            break;
        case NEUTRAL:
            if (Correct == 0 || Correct == 1)
                corrNeutral = corrNeutral + Correct;
            CEnvironment::Instance()->outputMessage("\nNeutral Accuracy: %f ", corrNeutral/nNeutral);
            break;
        default:
            break;
    }

		//trial type 
		storeTrialVariable("trialType", trialType);		
		storeTrialVariable("flashLocation", flashLoc);
		storeTrialVariable("respLocation", respLoc);

		// cue target timing 
		storeTrialVariable("SOA", SOA);
		storeTrialVariable("targetEcc", targetEcc);//px
		storeTrialVariable("subject", m_paramsFile->getString(CFG_SUBJECT));
		storeTrialVariable("RGB", m_paramsFile->getInteger(CFG_RGB));
		
		// save information about the test calibration
		storeTrialVariable("testCalibration", testCalibration);
		storeTrialVariable("xOffset", xShift);
		storeTrialVariable("yOffset", yShift); //px

		storeTrialVariable("debug", debug); 
		storeTrialVariable("cueOnly", cueOnly);
		saveTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT) + "/");
		// saveDebugTrial("./Data/" + m_paramsFile->getString(CFG_SUBJECT));

		//update progress through session
		nTrial++;

		
		// recalibration active at each trial
		if (!debug){
			xPos = 0;
			yPos = 0;
			//xShift = 0; 
			//yShift = 0; 
			testCalibration = true;
			hasCalibrated = false;
			nRecal++;
			m_timerCheck.start(100);
			m_whitebox->pxSetPosition(0,0);
			m_whitebox->show();
	    }
		CEnvironment::Instance()->outputMessage("-----------------------------------------------------");

		//these are all the trials with no recalibration
		if (debug){
			m_pause.start(1000);
			testCalibration = false;
			hideAllObjects();
		}
		////all trials with recalibration
		//else{
		//	// keep track of the test calibration trials
		//	nRecal++;
		//	testCalibration = true;

		//}
		gotoFixation();
}
