frequency = 1;
phase = [0 90 180 270];
amplitude = 1;
[X,Y]= meshgrid(0:0.01:1,0:0.01:1);
for i = 1:length(phase)
    Z = 0.0001*sin((2*3.1415*frequency.*X)+phase(i));
    cVec = interp1([0 1], [0.46 0.54], (0.:0.01:1));
    cmap = [cVec' cVec' cVec'];
    figure('rend', 'painters', 'pos',[0 0 200 200]);
    surf(X,Y,Z)
    shading interp
    view(-45,90)
    set(gcf, 'Colormap', cmap)
    shading interp
    axis off
    axis square
    saveas(gca, sprintf('./1F%iD6C.bmp', i));
end

%% use gabor function
% for window of 25 px, high - 6.4, low - 19
% low frequency: amplitude = [1 0.6 0.4 0.2 0.15 0.1 0.075 0.05];
% high frequency: amplitude = [1 0.8 0.6 0.5 0.4 0.3 0.2 0.1];
amplitude = [0.375];
wavelength = 6.4;
orientation = 315;
phase = [90 270];
width = 25;
height = 25;
sigma = 4.5;
window = 'gaussian';
for i = 1:length(amplitude)
    for j = 1:length(phase)
        [patch, mask] = Gabor( wavelength, orientation, phase(j), amplitude(i), width, height, window, sigma );
        patch = patch + 0.5; % maximize so the background is grey
        rgbImage = cat(3, patch, patch, patch); % from grey scale to RGB
        figure('pos',[0 0 width height]);
        %imshow(patch)
        % just naming things
        if wavelength <  10
            nFreq = 2;
        else
            nFreq = 1;
        end
        %nCont = round((1-amplitude(i)) / 0.2);
        imwrite(rgbImage,sprintf('./%iF%iD%iA.bmp', nFreq, phase(j), amplitude(i)*1000))
    end
end