function list = List()

% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');

list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');

list = eis_readData(list, 'stream', 0, 'double');
list = eis_readData(list, 'stream', 1, 'double');
list = eis_readData(list, 'stream', 2, 'double');
list = eis_readData(list, 'stream', 3, 'double');

        
%----------------------------------------------------------
%Test Calibration
list = eis_readData(list, 'uservar','testCalibration');
list = eis_readData(list, 'uservar','xOffset');
list = eis_readData(list, 'uservar','yOffset');

%time of the response (locked to the start of the trial)		
list = eis_readData(list, 'uservar','timeFlashON');
list = eis_readData(list, 'uservar','timeFlashOFF');
list = eis_readData(list, 'uservar','timeFixON');
list = eis_readData(list, 'uservar','timeFixOFF');
list = eis_readData(list, 'uservar','timeTargetON');
list = eis_readData(list, 'uservar','timeTargetOFF');
list = eis_readData(list, 'uservar','timeRespCueOFF');

% experiment variables
list = eis_readData(list, 'uservar','pxAngle');
list = eis_readData(list, 'uservar','targetFrequency');
list = eis_readData(list, 'uservar','isGabor');
list = eis_readData(list, 'uservar','targetEcc');
list = eis_readData(list, 'uservar','trialType');
list = eis_readData(list, 'uservar','flashLocation');
list = eis_readData(list, 'uservar','respLocation');
list = eis_readData(list, 'uservar','SOA');

list = eis_readData(list, 'uservar','t1Tilt');
list = eis_readData(list, 'uservar','t2Tilt');
list = eis_readData(list, 'uservar','t1Phase');
list = eis_readData(list, 'uservar','t2Phase');
% subject performance
list = eis_readData(list, 'uservar','correct');
list = eis_readData(list, 'uservar','response');
list = eis_readData(list, 'uservar','reactionTime');

% target rgb color
list = eis_readData(list, 'uservar','RGB');
list = eis_readData(list, 'uservar','subject');



%----------------------------------------------------------
