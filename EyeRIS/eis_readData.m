%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Select a data field for retrival from the EYERIS structure. The function 
% accepts the following standard fields:
%
%     spf          - Add the number of samples per frame 
%
%     vchannel     - Add the virtual channel specified by Arg1
%                    Valid argument: i (0 <= i < 10)
%                    When no specified, all virtual channels are added
%
%     x            - Add the x component of the eye1 position (monocular)
%     x1           - Add the x component of the eye1 position (binocular)
%     x2           - Add the x component of the eye2 position (binocular)
%
%     y            - Add the y component of the eye1 position (monocular)
%     y1           - Add the y component of the eye1 position (binocular)
%     y2           - Add the y component of the eye2 position (binocular)
%
%     trigger      - Add a trigger specified by Arg1
%                    Valid argument 1: blink, notrack, 
%                    frame, sync, white, dropped,
%                    bad_data1, eoe1, em_event1, fixation1, lfixation1, 
%                    saccade1, msaccade1,
%                    bad_data2, eoe2, em_event2, fixation2, lfixation2, 
%                    saccade2, msaccade2,
%                    i (0 <= i < 11).
%                    When no trigger is specified, then all available 
%                    triggers are included
%
%     joypad       - Add the a joypad's button specified by Arg1
%                    Valid argument 1: slct, joyl, joyr, 
%                    strt, up, rght, down, left, l2, r2, l1, r1, 
%                    triangle, circle, x, square.
%                    When no button is specified all available joypad 
%                    information is included
%
%     stream       - Add an user stream specified by Arg1, and casted
%                    to the type specified by Arg2. If no Arg1 is specified
%                    all streams are imported and no cast is performed.
%                    Valid argument 1: i (0 <= i < 4)
%                    Valid argument 2: bool, int, double
%
%     dii          - Add the DII stream
%
%     uart         - Add the UART stream
%
%     uservar      - Add a user variable 
%
%     date         - Add the date of the trial
%     trial        - Add the trial number
%     filename     - Add the file name of the trial
%     binocular    - Add the type of experiment (0) monocular (1) binocular
%     photocell    - Add the photocell signal
%
%
% RuleList: List of rules to add the new rule to
% Field   : Name of the field to add
%
% RuleList: Updated rule list
%
function RuleList = eis_readData(RuleList, Field, varargin)

  % Definition of the virtual channels in EOS
	EOS_VCHANNELS = 10;   % Number of available virtual channels
  EOS_USERSTREAMS = 4;  % Number of available user streams
  
	EOS_VCHANNEL_X1 = 0;	% Virtual channel of the X eye position
	EOS_VCHANNEL_Y1 = 1;	% Virtual channel of the Y eye position
	EOS_VCHANNEL_X2 = 2;	% Virtual channel of the X eye position
	EOS_VCHANNEL_Y2 = 3;	% Virtual channel of the Y eye position
	EOS_VCHANNEL_4 = 4;   %#ok<NASGU>
	EOS_VCHANNEL_5 = 5;   %#ok<NASGU>
	EOS_VCHANNEL_6 = 6;   %#ok<NASGU>
	EOS_VCHANNEL_7 = 7;   %#ok<NASGU>
	EOS_VCHANNEL_8 = 8;   %#ok<NASGU>
	EOS_VCHANNEL_PHOTOCELL = 9;	% Virtual channel of the photocell signal

  TAGFILE_BOOL = 1;
  TAGFILE_INT = 6;
  TAGFILE_DOUBLE = 9;
 
  % Create the correct rule
  switch lower(Field)

   % Add the samples per frame stream
   case 'spf'
     RuleList = eis_addRule(RuleList, 'spf', 'copy', 'spf');

   % Add the virtual channel specified by Arg1
   case 'vchannel'
 		 if (~isempty(varargin))
       Arg = varargin{1};
     
       if (Arg >= 0 && Arg < EOS_VCHANNELS)
         Name = sprintf('vchannel%02d', Arg);
         RuleList = eis_addRule(RuleList, 'vchannel', 'reduce', ...
           'vchannels', Arg + 1, Name);
       else
         error('Virtual channel not defined');
       end
     else
       RuleList = eis_addRule(RuleList, 'vchannel', 'copy', 'vchannels');
     end

   % Add the x/x1 component of the eye1 position (monocular/binocular)
   case 'x'
     RuleList = eis_addRule(RuleList, 'x', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_X1 + 1, 'x');

   case 'x1'
     RuleList = eis_addRule(RuleList, 'x1', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_X1 + 1, 'x1');

   % Add the x component of the eye2 position (binocular)
   case 'x2'
     RuleList = eis_addRule(RuleList, 'x2', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_X2 + 1, 'x2');
   
   % Add the y/y1 component of the eye1 position (monocular/binocular)
   case 'y'
     RuleList = eis_addRule(RuleList, 'y', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_Y1 + 1, 'y');

   case 'y1'
     RuleList = eis_addRule(RuleList, 'y1', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_Y1 + 1, 'y1');
     
   % Add the y component of the eye2 position (binocular)  
   case 'y2'
     RuleList = eis_addRule(RuleList, 'y2', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_Y2 + 1, 'y2');
     
   % Add the photocell signal
   case 'photocell'
     RuleList = eis_addRule(RuleList, 'photocell', 'reduce', 'vchannels', ...
       EOS_VCHANNEL_PHOTOCELL + 1, 'photocell');

	 % Add the trial number
   case 'trial'
     RuleList = eis_addRule(RuleList, 'trial', 'copy', 'trial');
     RuleList = eis_addRule(RuleList, 'trial', 'group', 'trial', 'info');

	 % Add the date of the trial
   case 'date'
     RuleList = eis_addRule(RuleList, 'date', 'copy', 'date');
     RuleList = eis_addRule(RuleList, 'date', 'group', 'date', 'info');

	 % Add the file name of the trial
   case 'filename'
     RuleList = eis_addRule(RuleList, 'filename', 'copy', 'filename');
     RuleList = eis_addRule(RuleList, 'filename', ...
       'group', 'filename', 'info');

	 % Add the type of trial (binocular/monocular)
   case 'binocular'
     RuleList = eis_addRule(RuleList, 'binocular', 'copy', 'binocular');
     RuleList = eis_addRule(RuleList, 'binocular', ...
       'group', 'binocular', 'info');

	 % Add all triggers
   case 'trigger'
		 if (isempty(varargin))
			 RuleList = eis_addRule(RuleList, 'trigger (all)', ...
         'copy', 'triggers');
     else
	     RuleList = p_eis_addTriggers(RuleList, varargin{1});
     end

   % Add the joypad information
   case 'joypad'
     if (isempty(varargin))
       RuleList = eis_addRule(RuleList, 'joypad (all)', ...
         'copy', 'joypad_data');
     else
       RuleList = p_eis_addJoypad(RuleList, varargin{1});
     end
     
   % Add the user streams specified by Arg1
   case 'stream'

     % Add all streams
     if isempty(varargin)
       for i = 0:EOS_USERSTREAMS - 1
         Name = sprintf('stream%02d', i);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_data']);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_data'], Name, 'data');

         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_ts']);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_ts'], Name, 'ts');

         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_size']);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_size'], Name, 'size');
       end
     elseif length(varargin) >= 1
       Arg = varargin{1};
       
       % Convert the requested type into something
       % compatible with MATLAB
       switch (varargin{2})
         case 'int'
           Type = TAGFILE_INT;
         case 'bool'
           Type = TAGFILE_BOOL;
         case 'double'
           Type = TAGFILE_DOUBLE;
       end
       Type = p_eis_datatype2MATLAB(Type);

       % Add only the specified stream
       if (Arg >= 0 && Arg < EOS_USERSTREAMS)
         Name = sprintf('stream%02d', Arg);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_data']);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_ts']);
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'copy', [Name '_size']);

         % If a typecast is requested, convert the data
         % and change the size to match the new type
         if length(varargin) == 2
           RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
             'cast', [Name '_data'], Type);
           [EyeRIS, DTSize] = p_eis_MATLAB2datatype(Type);
           RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
             '1func', [Name '_size'], 'f = f/a', DTSize);
         end

         % Group them under one single struct
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_data'], Name, 'data');
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_ts'], Name, 'ts');
         RuleList = eis_addRule(RuleList, ['stream ' Name ')'], ...
           'group', [Name '_size'], Name, 'size');
       end
     else
       error('User stream not defined');
     end

   % Add the dii stream
   case 'dii'
     RuleList = eis_addRule(RuleList, 'dii', 'copy', 'dii_data');
     RuleList = eis_addRule(RuleList, 'dii', 'copy', 'dii_ts');
     RuleList = eis_addRule(RuleList, 'dii', 'copy', 'dii_size');

     RuleList = eis_addRule(RuleList, 'dii', ...
       'group', 'dii_data', 'dii', 'data');
     RuleList = eis_addRule(RuleList, 'dii', ...
       'group', 'dii_ts', 'dii', 'ts');
     RuleList = eis_addRule(RuleList, 'dii', ...
       'group', 'dii_size', 'dii', 'size');

   % Add the uart stream
   case 'uart'
     RuleList = eis_addRule(RuleList, 'uart', 'copy', 'uart_data');
     RuleList = eis_addRule(RuleList, 'uart', 'copy', 'uart_ts');
     RuleList = eis_addRule(RuleList, 'uart', 'copy', 'uart_size');

     RuleList = eis_addRule(RuleList, 'uart', ...
       'group', 'uart_data', 'dii', 'data');
     RuleList = eis_addRule(RuleList, 'uart', ...
       'group', 'uart_ts', 'dii', 'ts');
     RuleList = eis_addRule(RuleList, 'uart', ...
       'group', 'uart_size', 'dii', 'size');

   % Add a user variable
   case 'uservar'
     RuleList = eis_addRule(RuleList, ['uservar ' varargin{1}], ...
       'copy', varargin{1});
     RuleList = eis_addRule(RuleList, ['uservar ' varargin{1}], ...
       'group', varargin{1}, 'user');

   otherwise
      error('Unknown field');
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add a new field for the joypad reduction to the list used to transform 
% an EYERIS structure to a user-defined structure. The function accepts 
% these buttons:
%
%   slct, joyl, joyr, strt, up, right, down, left, l2
%   r2, l1, r1, triangle, circle, x, square
%
% RuleList: List of rules to add the new rule to
% Button  : Name of the button to add
%
% RuleList: Updated rule list
%
function RuleList = p_eis_addJoypad(RuleList, Button)
  
  Mask1 = struct();
  Mask1.slct = '01';      % Select button
  Mask1.joyl = '02';      % Left joypad push
  Mask1.joyr = '04';      % Right joypad push
  Mask1.strt = '08';      % Start button
  Mask1.up = '10';        % Up button
  Mask1.rght = '20';      % Right button
  Mask1.down = '40';      % Down button
  Mask1.left = '80';      % Left button
  
  Mask2 = struct();
  Mask2.l2 = '01';        % L2 button
  Mask2.r2 = '02';        % R2 button
  Mask2.l1 = '04';        % L1 button
  Mask2.r1 = '08';        % R1 button
  Mask2.triangle = '10';	% Triangle button
  Mask2.circle = '20';    % Circle button
  Mask2.x	= '40';         % X button
  Mask2.square = '80';    % Square button

  % Evaluate the status of the first group of buttons
  if (isfield(Mask1, Button))
    Row = 5;
    Bit = Mask1.(Button);
  elseif (isfield(Mask2, Button))
    Row = 6;
    Bit = Mask2.(Button);
  else
    error(['Unknown joypad button (' Button ')']);
  end

  % Add the necessary rules for the specified button
  RuleList = eis_addRule(RuleList, ['joypad (' Button ')'], ...
    'reduce', 'joypad_data', Row, Button);
  RuleList = eis_addRule(RuleList, ['joypad (' Button ')'], ...
    '1func', Button, 'f = bitand(bitcmp(f, 8), hex2dec(a)) ~= 0', Bit);
  RuleList = eis_addRule(RuleList, ['joypad (' Button ')'], ...
    'group', Button, 'joypad');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add a new field for the triggers reduction to the list used to transform 
% an EYERIS structure to a user-defined structure. The function accepts 
% these triggers:
%
%   i (0 <= i < 11), 
%   bad_data1, eoe1, em_event1, fixation1, lfixation1, saccade1, msaccade1,
%   bad_data2, eoe2, em_event2, fixation2, lfixation2, saccade2, msaccade2,
%   dropped, white, notrack, blink, 
%   sync, frame
%
% RuleList   : List of rules to add the new rule to
% TriggerName: Name of the button to add
%
% RuleList   : Updated rule list
%
function RuleList = p_eis_addTriggers(RuleList, TriggerName)
  
	EOS_USER_TRIGGERS = 12;   % Number of available user triggers

  % Define the mask for the triggers
  Triggers = struct();
  
  Triggers.bad_data1 = '00001000';
  Triggers.eoe1 = '00002000';
  Triggers.em_event1 = '00004000';
  Triggers.fixation1 = '00008000';
  Triggers.lfixation1 = '00010000';
  Triggers.saccade1 = '00020000';
  Triggers.msaccade1 = '00040000';

  Triggers.bad_data2 = '00080000';
  Triggers.eoe2 = '00100000';
  Triggers.em_event2 = '00200000';
  Triggers.fixation2 = '00400000';
  Triggers.lfixation2 = '00800000';
  Triggers.saccade2 = '01000000';
  Triggers.msaccade2 = '02000000';
  
  Triggers.notrack = '04000000';
  Triggers.blink = '08000000';
  
  Triggers.dropped = '10000000';
  Triggers.white = '20000000';
  Triggers.sync = '40000000';
  Triggers.frame = '80000000';

  % Add the user trigger specified by Arg1
  if isnumeric(TriggerName)
    if (TriggerName >= 0 && TriggerName < EOS_USER_TRIGGERS)
      TriggerName = sprintf('user%02d', TriggerName);
      Bit = dec2hex(bitshift(uint32(1), TriggerName), 8);
    else
      error('User trigger not defined');
    end
  elseif (isfield(Triggers, TriggerName))
    Bit = Triggers.(TriggerName);
  else
    error(['Unknown trigger (' TriggerName ')']);
  end

  % Add the necessary rules for the specified trigger
  RuleList = eis_addRule(RuleList, ['trigger (' TriggerName ')'], ...
    'copy', 'triggers', '', TriggerName);
  RuleList = eis_addRule(RuleList, ['trigger (' TriggerName ')'], ...
    '1func', TriggerName, 'f = bitand(f, hex2dec(a)) ~= 0', Bit);
  RuleList = eis_addRule(RuleList, ['trigger (' TriggerName ')'], ...
    'group', TriggerName, 'triggers');
end