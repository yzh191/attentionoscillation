clc; clear; %close all;

% NS: 0 -> 90
% Z031: 75
% Z042: 75
% Z066: 50 -> 50
% Z095: 50
cd C:\Users\ruccilab\Box\APLab-Projects\AttentionSF\EyeRIS
addpath 'C:\Users\ruccilab\Box\APLab-Projects\SpatialMs\EyeRIS'

subject = {'SM2'};
MaxMSaccAmp = 30;
DDPI = 1;


for i = 1:length(subject)
    pt = sprintf('./%s/', subject{i});
    data = readdata2(pt, List);
    vt = preprocessDDPI(data, MaxMSaccAmp, 330);
    vt = convertSamplesToMs(vt, 1000/330);
    %save(sprintf('../ValidTrials/%s.mat', subject{i}), 'vt', 'data')
    save(sprintf('../ValidTrials/%s.mat', subject{i}), 'vt', 'data')
end

