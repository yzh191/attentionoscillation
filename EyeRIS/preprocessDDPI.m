% 
% This file is a modified version of Preprocess_Data()
% It takes a variable containing a list of the entries
% to implement.
% 
% 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Process eye movements data and create a list of trials.
% Only the period of stimulus presentation is examined.
% Eye movements are classified for all trials. No selection of trials is
% operated.
%
%  [ValidTrials] = PreprocessTrials(data,NumberOfTrials,MinSaccSpeed, MinSaccAmp,...
%                            MinMSaccSpeed, MinMSaccAmp, MaxMSaccAmp,RefractoryPeriod,Stabilized)
%%% Added stabilization Trials Data 11/12/2020

function [ValidTrials] = preprocessDDPI(data,MaxMSaccAmp, samplingRate)
%     data,NumberOfTrials,MinSaccSpeed, MinSaccAmp,...
%     MinMSaccSpeed, MinMSaccAmp, MaxMSaccAmp,MinVelocity);

% THESE PARAMS SHOULD BE DEFAULT TO BE OVERWRITTEN ONLY
% WHEN A NEW SET OF PARAMS IS SPECIFIED
 
MinSaccSpeed = 180;
MinSaccAmp = 30;
MinMSaccSpeed = 180;
MinMSaccAmp = 3;
%MaxMSaccAmp = 30;
MinVelocity = 120;
NumberOfTrials = length(data.x);

fprintf('\t Preprocessing trials\n');

ValidTrials = {};
ValidTrialsCounter = 0;

for TrialNumber = 1:NumberOfTrials
    DataValid = [];
    % Get the data relative to the trial

    X = (data.x{TrialNumber});
    Y = (data.y{TrialNumber});
    
    Blink = (data.triggers{TrialNumber}.blink);
    NoTrack = (data.triggers{TrialNumber}.notrack);
    DataValid = ones(1,length(data.x{TrialNumber}));
    DataInvalid = zeros(1,length(data.x{TrialNumber}));

    Trial = createTrial(TrialNumber, X, Y, Blink, NoTrack, DataValid, DataInvalid, samplingRate);
    
                            
    Trial = preprocessSignals(Trial, 'minvel',MinVelocity, 'noanalysis'); 
% % % %       Trial = preprocessSignalsDDPIHack(Trial, 'minvel',MinVelocity); 
    %%%% 'noanalysis' keeps the saccades at the very onset of the trial

    
    
    % Find all valid saccades with speed greater than 3 deg/sec
    % and bigger than 30 arcmin
    Trial = findSaccades(Trial, 'minvel',MinSaccSpeed, 'minsa',MinSaccAmp);

    % Find all valid microsaccades with speed greater than 3 deg/sec
    % and amplitude included in 3 arcmin and 60 arcmin
    Trial = findMicrosaccades(Trial, 'minvel',MinMSaccSpeed,'minmsa', MinMSaccAmp,'maxmsa', MaxMSaccAmp);
    Trial = findDrifts(Trial);
 
    fn=fieldnames(data.user{TrialNumber});
    
    %% for stabilized traces
    XposStab = data.stream00{TrialNumber}.data(2:end); %data
    YposStab = data.stream01{TrialNumber}.data(2:end);
    XTS = data.stream00{TrialNumber}.ts(2:end); %times **maybe time bin issue
    YTS = data.stream01{TrialNumber}.ts(2:end);

    xStab = eis_expandVector(XposStab, XTS, length(X), 'replica');
    yStab = eis_expandVector(YposStab, YTS, length(X), 'replica');
    %% Loop through and add more conditions

    for ff=1:size(fn,1)
        entry = char(fn(ff));
        Trial.(entry) = data.user{TrialNumber}.(entry);
    end
    
    Trial.xStab.position = xStab(1:end).*Trial.pxAngle; %convert stab to arcmin
    Trial.yStab.position = yStab(1:end).*Trial.pxAngle;
    Trial.xStab.stream = XposStab(1:end).*Trial.pxAngle; %convert stab to arcmin
    Trial.yStab.stream = YposStab(1:end).*Trial.pxAngle;
    Trial.xStab.ts = XTS; %convert stab to arcmin
    Trial.yStab.ts = YTS;
    %Trial.FileName = data.FileName{TrialNumber};
    
    ValidTrials(length(ValidTrials) + 1) = {Trial};
    ValidTrialsCounter = ValidTrialsCounter + 1;

end
