clear;clc;close all
merge = 0;
RGBs = [125 375];
%YZ = [100 600];
%RL - [6, 5]; was 5 4
%SJ - [6, 5];
% SM - [125 375];
frequency = {'low', 'high'};
SOAs = [100 500];

if ~merge
    [file, path] = uigetfile("./ValidTrials/*.mat");
    load([path file])
    session = str2double(regexp(file, '\d*', 'Match'));
else
    % if merge, concatenate vts
    [file, path] = uigetfile("./ValidTrials/*.mat", 'Select One or More Files', 'Multiselect', 'on');
    tmp = [];
    for i = 1:length(file)
        load([path file{i}])
        tmp = [tmp vt];
        clear vt
    end
    vt = tmp;
    clear tmp
    session = 0; % if merging all trials, session number is 0
end
%extract session and subject
stringCell = regexp(file, '\D*', 'Match');
subject = stringCell{1};
badEM = {'noResp', 'rtInvalid', 'gazeOff', 'blinks','microsaccades', 'saccades', 'invalid'};
CUTOFF = 30;

%% Filter
dtMat = zeros(length(vt), length(badEM));
nFt = 0;
figure()
for i = 1:length(vt)
    vt{i}.rt = vt{i}.reactionTime - (vt{i}.timeRespCueOFF - 500);
    t1 = vt{i}.timeFlashON - CUTOFF;
    t2 = vt{i}.timeRespCueOFF;
    if (ddpiTime(t2) > length(vt{i}.x.position)) || (t1 < 0) 
        kill = 1;
        continue
    else
        [dtMat(i, :), kill] = filterVt(vt{i}, t1, t2, badEM);
    end
    % double check the traces
    %     if kill
    %         kill = plotEyeTrace(vt, i);
    %     end
    if ~kill
        %plotEyeTrace(vt, i)
        nFt = nFt + 1;
        ft.correct(nFt) = vt{i}.correct;
        ft.SOA(nFt) = vt{i}.SOA;
        ft.freq(nFt) = vt{i}.targetFrequency;
        ft.trial(nFt) = vt{i}.trialType;
        ft.rt(nFt) = vt{i}.rt;
        ft.target(nFt) = vt{i}.(sprintf('t%iTilt', vt{i}.respLocation));
        ft.response(nFt) = vt{i}.response;
        ft.RGB(nFt) = vt{i}.RGB;
        ft.respLoc(nFt) = vt{i}.respLocation;
        ft.flashLoc(nFt) = vt{i}.flashLocation;
        ft.xOffset(nFt) = vt{i}.xOffset;
        ft.yOffset(nFt) = vt{i}.yOffset;
    end
end

% construct dt see what is discarded
dtTable = array2table(sum(dtMat),'VariableNames', badEM)
%calculate trial numbers
nTotal = length(vt);
nDt = sum(sum(dtMat, 2));
%concole printout
fprintf('\ntotal:\t\t %i \ndiscard:\t %i \nfilter:\t\t %i (prop: %.2f)', nTotal, nDt, nFt, nFt/nTotal);

% process filtered data
for freqIdx = 1:2
    for SOAIdx = 1:length(SOAs)
        for trialIdx = 1:3
            % save target and response for dprime and z-test
            list(SOAIdx, trialIdx, freqIdx).target = ft.target(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx));
            list(SOAIdx, trialIdx, freqIdx).response = ft.response(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx));
            stats.d(SOAIdx, trialIdx, freqIdx) = dprime(list(SOAIdx, trialIdx, freqIdx).response, list(SOAIdx, trialIdx, freqIdx).target);
            %stats struct (trial type, SOA, spatial frequency)
            stats.perf(SOAIdx, trialIdx, freqIdx) = ...
                mean(ft.correct(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx)));
            stats.ci_perf(SOAIdx, trialIdx, freqIdx) = ...
                ciPerf(ft.correct(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx)));
            stats.rt(SOAIdx, trialIdx, freqIdx) = ...
                mean(ft.rt(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx)));
            stats.ci_rt(SOAIdx, trialIdx, freqIdx) = ...
                nansem(ft.rt(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx)), 2);
            stats.count(SOAIdx, trialIdx, freqIdx) = ...
                sum(ft.trial == trialIdx & ft.SOA == SOAs(SOAIdx) & ft.freq == freqIdx & ft.RGB == RGBs(freqIdx));
        end
    end
end

try
    save(sprintf('./FilterTrials/%s%i.mat', subject, session), 'ft')
catch
    save(sprintf('./FilterTrials/%s%i.mat', subject{1}, session), 'ft')
end

%% z-test
zStats = [];
for freqIdx = 1:2
    for SOAIdx = 1:2
        tmp = [];
        [~,~,~,~,tmp(1)] = zTest(list(SOAIdx, 1, freqIdx).response, list(SOAIdx, 1, freqIdx).target, ...
            list(SOAIdx, 2, freqIdx).response, list(SOAIdx, 2, freqIdx).target)
        [~,~,~,~,tmp(2)] = zTest(list(SOAIdx, 1, freqIdx).response, list(SOAIdx, 1, freqIdx).target, ...
            list(SOAIdx, 3, freqIdx).response, list(SOAIdx, 3, freqIdx).target)
        [~,~,~,~,tmp(3)] = zTest(list(SOAIdx, 2, freqIdx).response, list(SOAIdx, 2, freqIdx).target, ...
            list(SOAIdx, 3, freqIdx).response, list(SOAIdx, 3, freqIdx).target)
        zStats = [zStats; tmp];
        %stats z struct (row - SOA, column - freq)
        %             [~, ~, ~, ~, z.CvsI(SOAIdx, freqIdx)] = zTest(list(SOAIdx, 1, freqIdx).response, list(SOAIdx, 1, freqIdx).target, ...
        %                 list(SOAIdx, 2, freqIdx).response, list(SOAIdx, 2, freqIdx).target);
        %             [~, ~, ~, ~, z.IvsN(SOAIdx, freqIdx)] = zTest(list(SOAIdx, 2, freqIdx).response, list(SOAIdx, 2, freqIdx).target, ...
        %                 list(SOAIdx, 3, freqIdx).response, list(SOAIdx, 3, freqIdx).target);
        %             [~, ~, ~, ~, z.CvsN(SOAIdx, freqIdx)] = zTest(list(SOAIdx, 1, freqIdx).response, list(SOAIdx, 1, freqIdx).target, ...
        %                 list(SOAIdx, 3, freqIdx).response, list(SOAIdx, 3, freqIdx).target);
        
    end
end
zTable = array2table(zStats, 'VariableNames', {'Cong. vs Incong.', 'Cong. vs Neut.', 'Incong. vs Neut.'}, ...
    'RowNames', {'LowSOA100', 'LowSOA500', 'HighSOA100', 'HighSOA500'})

tableParams.dataFormat = {'%.2f', 3};
tableParams.completeTable = 1;
tableParams.transposeTable = 0;
tableParams.tableLabel = sprintf('%sZTest', subject{1});
tableParams.tableCaption = sprintf('%s z test', subject{1});
tableParams.tableBorders = 1;
dataTable = array2table(zStats, 'rowNames', {'LowSOA100', 'LowSOA500', 'HighSOA100', 'HighSOA500'}, ...
    'VariableNames', {'Cong. vs Incong.', 'Cong. vs Neut.', 'Incong. vs Neut.'});
finalTableOutput(dataTable, tableParams, './IndvTable/', sprintf('%sZTest.txt', subject{1}))
%% plotting one SOA
freqIdx = 2;
vars = {'rt', 'perf'};
color = pastel9(length(SOAs));
for varIdx = 1:length(vars)
    varName = vars{varIdx};
    figure('rend','painters','pos',[0 0 600 700]);
    for SOAIdx = 1:length(SOAs)
        hold on
        errorbar(stats.(varName)(SOAIdx, :, freqIdx), stats.(sprintf('ci_%s', varName))(SOAIdx, :, freqIdx), ...
            'Color', color(SOAIdx, :), 'Marker', '.', 'LineWidth', 2.5, 'MarkerSize', 30, 'CapSize', 10)
        for t = 1:3
            text(t, stats.(varName)(SOAIdx, t, freqIdx) + 0.1, sprintf('n = %i', stats.count(SOAIdx, t, freqIdx)), ...
                'color', color(SOAIdx, :), 'FontSize', 20)
        end
    end
    xlim([0 4])
    if strcmp(varName, 'perf')
        ylim([0.5 1])
    elseif strcmp(varName, 'rt')
        ylim ([400 800])
        yticks(400:100:800)
    end
    xticks(1:3)
    legend({'100', '500'}, 'location', 'best')
    xticklabels({'Cong.', 'Incong', 'Neutral'})
    xtickangle(45)
    ylabel(getLabel(varName))
    title(sprintf('%s%i', frequency{freqIdx}, session))
    set(gca, 'linewidth', 3, 'fontsize', 32)
    % saveas(gca, sprintf('./IndvFigs/%s/%s%sBox.png', subject{1}, varName, frequency{freqIdx}));
    try
        saveas(gca, sprintf('./IndvFigs/%s/%s%s%i.png', subject, varName, frequency{freqIdx}, session));
    catch
        saveas(gca, sprintf('./IndvFigs/%s/%s%sAmp%i.png', subject{1}, varName, frequency{freqIdx}, RGBs(freqIdx)));
    end
end

%% plotting all SOAs
freqIdx = 2;
varName = 'perf';
color = pastel9(length(SOAs));
figure('rend','painters','pos',[0 0 600 700]);
plot(SOAs, stats.perf(:, :, 2)', 'LineWidth', 2.5)
xlim([SOAs(1) - 50 SOAs(end)+50])
xticks(SOAs)
legend({'Cong.', 'Incong', 'Neutral'}, 'location', 'best')
ylabel('Proportion correct')
title(sprintf('%s%i', frequency{freqIdx}, session))
set(gca, 'linewidth', 3, 'fontsize', 32)
try
    saveas(gca, sprintf('./IndvFigs/%s/%sTL%i.png', subject, frequency{freqIdx}, session));
catch
    saveas(gca, sprintf('./IndvFigs/%s/%sTLAmp%i.png', subject{1}, frequency{freqIdx}, RGBs(freqIdx)));
end